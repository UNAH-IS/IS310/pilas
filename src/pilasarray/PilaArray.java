/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilasarray;

/**
 *
 * @author enrique
 */
public class PilaArray {
    //private static final int TAMPILA = 10;
    private int cima;
    private final Object[] lista;
    private int tamañoPila;
    
    public PilaArray(int tamañoPila) {
        this.tamañoPila = tamañoPila;
        this.lista = new Object[tamañoPila];
        this.cima = -1;
    }
    
    public boolean pilaLlena() {
        return cima == this.tamañoPila - 1;
    }
    
    public boolean pilaVacia() {
        return cima == -1;
    }
    
    public void push(Object elemento) throws Exception {
        if (pilaLlena()) {
            throw new Exception("Desbordamiento de Pila");
        } else {
            this.cima++;
            this.lista[cima] = elemento;
        }
    }
    
    public Object pop() throws Exception {
        if (pilaVacia()) {
            throw new Exception ("Pila vacia");
        } else {
            Object aux = this.lista[cima];
            this.cima--;
            
            return aux;
        }
    }
    
    public Object getCima() throws Exception {
        if (pilaVacia()) {
            throw new Exception("Pila esta vacia");
        } else {
            return this.lista[cima];
        }
    }
    
    public void vaciarPila() {
        this.cima = -1;
    }
}
