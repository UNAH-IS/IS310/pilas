/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilasarray;

import java.util.Scanner;

/**
 *
 * @author enrique
 */
public class PilaArrayTest {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int tamaño;
        boolean continuar = true;

        System.out.print("Escriba el tamaño de la pila: ");

        try {
            tamaño = Integer.valueOf(entrada.nextLine());
        } catch (Exception ex) {
            tamaño = 10;
        }

        PilaArray pila = new PilaArray(tamaño);
        /*do {
            try {

                System.out.println("Colocar un valor para la cima: ");
                Object dato = entrada.nextLine();
                pila.push(dato);

                System.out.printf("Esta vacia: %s%n", pila.pilaVacia());
                System.out.printf("Esta llena: %s%n", pila.pilaLlena());

            } catch (Exception ex) {
                continuar = false;
            }
        } while (continuar);*/

        try {
            pila.push("hola");
            pila.push(134425);
            pila.push(true);

            //Object excima = pila.pop();
            pila.pop();
            pila.pop();
            
            //System.out.printf("La cima anterior es: %s%n", excima);
            System.out.printf("La cima es: %s%n", pila.getCima());
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }
}
